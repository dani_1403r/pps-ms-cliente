package com.cliente;

import com.cliente.model.Cliente;
import com.cliente.model.Contacto;
import com.cliente.model.DatosPrincipal;
import com.cliente.model.Notificacion;

import java.util.ArrayList;
import java.util.List;


public class MockClientes {

    public static  List<Cliente> mockClientes = new ArrayList<>();

    public  void getMockClientes() {
        //List<Cliente> listaClientes = new ArrayList<Cliente>();
        DatosPrincipal datosPrincipal = new DatosPrincipal();
        datosPrincipal.setRut(247877565L);
        datosPrincipal.setPrimerNombre("Daniel");
        datosPrincipal.setPrimerApellido("Rodriguez");
        Contacto contacto = new Contacto();
        contacto.setTelefono("988373663");
        Cliente cliente = new Cliente();
        cliente.setDatosPrincipal(datosPrincipal);
        cliente.setContacto(contacto);
        mockClientes.add(cliente);
        /**Nuevo cliente
         *
         */
        datosPrincipal = new DatosPrincipal();
        datosPrincipal.setRut(256784786L);
        datosPrincipal.setPrimerNombre("Alfredo");
        datosPrincipal.setPrimerApellido("Rodriguez");
        contacto = new Contacto();
        contacto.setTelefono("98863789");
        cliente = new Cliente();
        cliente.setDatosPrincipal(datosPrincipal);
        cliente.setContacto(contacto);
        mockClientes.add(cliente);

    }

    public void setMockClientes(List<Cliente> mockClientes) {
        this.mockClientes = mockClientes;
    }



}
