package com.cliente.model;

import java.util.Objects;
import com.cliente.model.Contacto;
import com.cliente.model.DatosPrincipal;
import com.cliente.model.Direccion;
import com.cliente.model.FechaNacimiento;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Cliente
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-04-21T16:44:18.089-04:00[America/Santiago]")
public class Cliente   {
  @JsonProperty("datos_principal")
  private DatosPrincipal datosPrincipal;

  @JsonProperty("fecha_nacimiento")
  private FechaNacimiento fechaNacimiento;

  @JsonProperty("direccion")
  private Direccion direccion;

  @JsonProperty("contacto")
  private Contacto contacto;

  public Cliente datosPrincipal(DatosPrincipal datosPrincipal) {
    this.datosPrincipal = datosPrincipal;
    return this;
  }

  /**
   * Get datosPrincipal
   * @return datosPrincipal
  */
  @ApiModelProperty(value = "")

  @Valid

  public DatosPrincipal getDatosPrincipal() {
    return datosPrincipal;
  }

  public void setDatosPrincipal(DatosPrincipal datosPrincipal) {
    this.datosPrincipal = datosPrincipal;
  }

  public Cliente fechaNacimiento(FechaNacimiento fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
    return this;
  }

  /**
   * Get fechaNacimiento
   * @return fechaNacimiento
  */
  @ApiModelProperty(value = "")

  @Valid

  public FechaNacimiento getFechaNacimiento() {
    return fechaNacimiento;
  }

  public void setFechaNacimiento(FechaNacimiento fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
  }

  public Cliente direccion(Direccion direccion) {
    this.direccion = direccion;
    return this;
  }

  /**
   * Get direccion
   * @return direccion
  */
  @ApiModelProperty(value = "")

  @Valid

  public Direccion getDireccion() {
    return direccion;
  }

  public void setDireccion(Direccion direccion) {
    this.direccion = direccion;
  }

  public Cliente contacto(Contacto contacto) {
    this.contacto = contacto;
    return this;
  }

  /**
   * Get contacto
   * @return contacto
  */
  @ApiModelProperty(value = "")

  @Valid

  public Contacto getContacto() {
    return contacto;
  }

  public void setContacto(Contacto contacto) {
    this.contacto = contacto;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Cliente cliente = (Cliente) o;
    return Objects.equals(this.datosPrincipal, cliente.datosPrincipal) &&
        Objects.equals(this.fechaNacimiento, cliente.fechaNacimiento) &&
        Objects.equals(this.direccion, cliente.direccion) &&
        Objects.equals(this.contacto, cliente.contacto);
  }

  @Override
  public int hashCode() {
    return Objects.hash(datosPrincipal, fechaNacimiento, direccion, contacto);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Cliente {\n");
    
    sb.append("    datosPrincipal: ").append(toIndentedString(datosPrincipal)).append("\n");
    sb.append("    fechaNacimiento: ").append(toIndentedString(fechaNacimiento)).append("\n");
    sb.append("    direccion: ").append(toIndentedString(direccion)).append("\n");
    sb.append("    contacto: ").append(toIndentedString(contacto)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

