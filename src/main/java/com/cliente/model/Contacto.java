package com.cliente.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Contacto cliente
 */
@ApiModel(description = "Contacto cliente")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-04-21T16:44:18.089-04:00[America/Santiago]")
public class Contacto   {
  @JsonProperty("email")
  @Email
  private String email = "correo@cliente.com";

  @JsonProperty("telefono")
  @Size(min=9,max=9, message = "La cantidad de numeros permitida es de 9 digitos")
  private String telefono = "912345678";


  @JsonProperty("contacto_emergencia")
  @Size(min=9,max=9, message = "La cantidad de numeros permitida es de 9 digitos")
  private String contactoEmergencia = "912345678";


  public Contacto email(String email) {
    this.email = email;
    return this;
  }

  /**
   * correo electronico del cliente
   * @return email
  */
  @ApiModelProperty(value = "correo electronico del cliente")
  @Valid


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Contacto telefono(String telefono) {
    this.telefono = telefono;
    return this;
  }

  /**
   * telefono del cliente
   * @return telefono
  */
  @ApiModelProperty(value = "telefono del cliente")
  @Valid


  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  public Contacto contactoEmergencia(String contactoEmergencia) {
    this.contactoEmergencia = contactoEmergencia;
    return this;
  }

  /**
   * telefono del cliente
   * @return contactoEmergencia
  */
  @ApiModelProperty(value = "telefono del cliente")
  @Valid


  public String getContactoEmergencia() {
    return contactoEmergencia;
  }

  public void setContactoEmergencia(String contactoEmergencia) {
    this.contactoEmergencia = contactoEmergencia;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Contacto contacto = (Contacto) o;
    return Objects.equals(this.email, contacto.email) &&
        Objects.equals(this.telefono, contacto.telefono) &&
        Objects.equals(this.contactoEmergencia, contacto.contactoEmergencia);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, telefono, contactoEmergencia);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Contacto {\n");
    
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    telefono: ").append(toIndentedString(telefono)).append("\n");
    sb.append("    contactoEmergencia: ").append(toIndentedString(contactoEmergencia)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

