package com.cliente.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * fecha de nacimiento
 */
@ApiModel(description = "fecha de nacimiento")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-04-21T16:44:18.089-04:00[America/Santiago]")
public class FechaNacimiento   {
  @JsonProperty("dia")
  private BigDecimal dia;

  @JsonProperty("mes")
  private BigDecimal mes;

  @JsonProperty("año")
  private BigDecimal año;

  /**
   * estado civil del cliente
   */
  public enum EstadoCivilEnum {
    SOLTERO("soltero"),
    
    CASADO("casado"),
    
    VIUDO("viudo"),
    
    DIVORCIADO("divorciado");

    private String value;

    EstadoCivilEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static EstadoCivilEnum fromValue(String value) {
      for (EstadoCivilEnum b : EstadoCivilEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("estado_civil")
  private EstadoCivilEnum estadoCivil;

  public FechaNacimiento dia(BigDecimal dia) {
    this.dia = dia;
    return this;
  }

  /**
   * dia
   * @return dia
  */
  @ApiModelProperty(value = "dia")

  @Valid

  public BigDecimal getDia() {
    return dia;
  }

  public void setDia(BigDecimal dia) {
    this.dia = dia;
  }

  public FechaNacimiento mes(BigDecimal mes) {
    this.mes = mes;
    return this;
  }

  /**
   * mes
   * @return mes
  */
  @ApiModelProperty(value = "mes")

  @Valid

  public BigDecimal getMes() {
    return mes;
  }

  public void setMes(BigDecimal mes) {
    this.mes = mes;
  }

  public FechaNacimiento año(BigDecimal año) {
    this.año = año;
    return this;
  }

  /**
   * año
   * @return año
  */
  @ApiModelProperty(value = "año")

  @Valid

  public BigDecimal getAño() {
    return año;
  }

  public void setAño(BigDecimal año) {
    this.año = año;
  }

  public FechaNacimiento estadoCivil(EstadoCivilEnum estadoCivil) {
    this.estadoCivil = estadoCivil;
    return this;
  }

  /**
   * estado civil del cliente
   * @return estadoCivil
  */
  @ApiModelProperty(value = "estado civil del cliente")


  public EstadoCivilEnum getEstadoCivil() {
    return estadoCivil;
  }

  public void setEstadoCivil(EstadoCivilEnum estadoCivil) {
    this.estadoCivil = estadoCivil;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FechaNacimiento fechaNacimiento = (FechaNacimiento) o;
    return Objects.equals(this.dia, fechaNacimiento.dia) &&
        Objects.equals(this.mes, fechaNacimiento.mes) &&
        Objects.equals(this.año, fechaNacimiento.año) &&
        Objects.equals(this.estadoCivil, fechaNacimiento.estadoCivil);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dia, mes, año, estadoCivil);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FechaNacimiento {\n");
    
    sb.append("    dia: ").append(toIndentedString(dia)).append("\n");
    sb.append("    mes: ").append(toIndentedString(mes)).append("\n");
    sb.append("    año: ").append(toIndentedString(año)).append("\n");
    sb.append("    estadoCivil: ").append(toIndentedString(estadoCivil)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

