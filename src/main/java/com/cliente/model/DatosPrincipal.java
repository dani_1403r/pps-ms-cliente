package com.cliente.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * DatosPrincipal
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-04-21T16:44:18.089-04:00[America/Santiago]")
public class DatosPrincipal   {
  @JsonProperty("rut")
  @NotEmpty(message = "El campo no debe estar vacio")
  @Size(min=8,max=9, message = "La cantidad de numeros permitida es de min 8 y max 9 digitos")
  private long rut;

  @JsonProperty("primer_nombre")
  @Size(min=3, message = "La cantidad de caracteres permitida es min 3 ")
  private String primerNombre;


  @JsonProperty("segundo_nombre")
  @Size(min=3, message = "La cantidad de caracteres permitida es min 3 ")
  private String segundoNombre;


  @JsonProperty("primer_apellido")
  @Size(min=3, message = "La cantidad de caracteres permitida es min 3 ")
  private String primerApellido;


  @JsonProperty("segundo_apellido")
  @Size(min=3, message = "La cantidad de caracteres permitida es min 3 ")
  private String segundoApellido;


  /**
   * tipo de sexo del cliente
   */
  public enum GeneroEnum {
    MASCULINO("masculino"),
    
    FEMENINO("femenino");

    private String value;

    GeneroEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static GeneroEnum fromValue(String value) {
      for (GeneroEnum b : GeneroEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("genero")
  private GeneroEnum genero;

  public DatosPrincipal rut(long rut) {
    this.rut = rut;
    return this;
  }

  /**
   * Get rut
   * @return rut
  */
  @ApiModelProperty(value = "")

  @Valid

  public long getRut() {
    return rut;
  }

  public void setRut(long rut) {
    this.rut = rut;
  }

  public DatosPrincipal primerNombre(String primerNombre) {
    this.primerNombre = primerNombre;
    return this;
  }

  /**
   * Get primerNombre
   * @return primerNombre
  */
  @ApiModelProperty(value = "")
  @Valid


  public String getPrimerNombre() {
    return primerNombre;
  }

  public void setPrimerNombre(String primerNombre) {
    this.primerNombre = primerNombre;
  }

  public DatosPrincipal segundoNombre(String segundoNombre) {
    this.segundoNombre = segundoNombre;
    return this;
  }

  /**
   * Get segundoNombre
   * @return segundoNombre
  */
  @ApiModelProperty(value = "")


  public String getSegundoNombre() {
    return segundoNombre;
  }

  public void setSegundoNombre(String segundoNombre) {
    this.segundoNombre = segundoNombre;
  }

  public DatosPrincipal primerApellido(String primerApellido) {
    this.primerApellido = primerApellido;
    return this;
  }

  /**
   * Get primerApellido
   * @return primerApellido
   * @param
  */
  @ApiModelProperty(value = "")


  public String getPrimerApellido() {
    return primerApellido;
  }

  public void setPrimerApellido(String primerApellido) {
    this.primerApellido = primerApellido;
  }

  public DatosPrincipal segundoApellido(String segundoApellido) {
    this.segundoApellido = segundoApellido;
    return this;
  }

  /**
   * Get segundoApellido
   * @return segundoApellido
  */
  @ApiModelProperty(value = "")


  public String getSegundoApellido() {
    return segundoApellido;
  }

  public void setSegundoApellido(String segundoApellido) {
    this.segundoApellido = segundoApellido;
  }

  public DatosPrincipal genero(GeneroEnum genero) {
    this.genero = genero;
    return this;
  }

  /**
   * tipo de sexo del cliente
   * @return genero
  */
  @ApiModelProperty(value = "tipo de sexo del cliente")


  public GeneroEnum getGenero() {
    return genero;
  }

  public void setGenero(GeneroEnum genero) {
    this.genero = genero;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DatosPrincipal datosPrincipal = (DatosPrincipal) o;
    return Objects.equals(this.rut, datosPrincipal.rut) &&
        Objects.equals(this.primerNombre, datosPrincipal.primerNombre) &&
        Objects.equals(this.segundoNombre, datosPrincipal.segundoNombre) &&
        Objects.equals(this.primerApellido, datosPrincipal.primerApellido) &&
        Objects.equals(this.segundoApellido, datosPrincipal.segundoApellido) &&
        Objects.equals(this.genero, datosPrincipal.genero);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rut, primerNombre, segundoNombre, primerApellido, segundoApellido, genero);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DatosPrincipal {\n");
    
    sb.append("    rut: ").append(toIndentedString(rut)).append("\n");
    sb.append("    primerNombre: ").append(toIndentedString(primerNombre)).append("\n");
    sb.append("    segundoNombre: ").append(toIndentedString(segundoNombre)).append("\n");
    sb.append("    primerApellido: ").append(toIndentedString(primerApellido)).append("\n");
    sb.append("    segundoApellido: ").append(toIndentedString(segundoApellido)).append("\n");
    sb.append("    genero: ").append(toIndentedString(genero)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

