package com.cliente.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * direccion del cliente
 */
@ApiModel(description = "direccion del cliente")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-04-21T16:44:18.089-04:00[America/Santiago]")
public class Direccion   {
  @JsonProperty("comuna")
  private String comuna;

  @JsonProperty("region")
  private String region;

  @JsonProperty("casa_depto")
  private String casaDepto;

  @JsonProperty("calle")
  private String calle;

  @JsonProperty("numero")
  private BigDecimal numero;

  @JsonProperty("pais")
  private String pais;

  @JsonProperty("nacionalidad")
  private String nacionalidad;

  @JsonProperty("ocupacion")
  private String ocupacion;

  public Direccion comuna(String comuna) {
    this.comuna = comuna;
    return this;
  }

  /**
   * comuna donde vive el cliente
   * @return comuna
  */
  @ApiModelProperty(value = "comuna donde vive el cliente")


  public String getComuna() {
    return comuna;
  }

  public void setComuna(String comuna) {
    this.comuna = comuna;
  }

  public Direccion region(String region) {
    this.region = region;
    return this;
  }

  /**
   * region donde vive el cliente
   * @return region
  */
  @ApiModelProperty(value = "region donde vive el cliente")


  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public Direccion casaDepto(String casaDepto) {
    this.casaDepto = casaDepto;
    return this;
  }

  /**
   * casa/depto donde vive el cliente
   * @return casaDepto
  */
  @ApiModelProperty(value = "casa/depto donde vive el cliente")


  public String getCasaDepto() {
    return casaDepto;
  }

  public void setCasaDepto(String casaDepto) {
    this.casaDepto = casaDepto;
  }

  public Direccion calle(String calle) {
    this.calle = calle;
    return this;
  }

  /**
   * calle donde vive el cliente
   * @return calle
  */
  @ApiModelProperty(value = "calle donde vive el cliente")


  public String getCalle() {
    return calle;
  }

  public void setCalle(String calle) {
    this.calle = calle;
  }

  public Direccion numero(BigDecimal numero) {
    this.numero = numero;
    return this;
  }

  /**
   * numero de calle donde vive el cliente
   * @return numero
  */
  @ApiModelProperty(value = "numero de calle donde vive el cliente")

  @Valid

  public BigDecimal getNumero() {
    return numero;
  }

  public void setNumero(BigDecimal numero) {
    this.numero = numero;
  }

  public Direccion pais(String pais) {
    this.pais = pais;
    return this;
  }

  /**
   * pais de residencia del cliente
   * @return pais
  */
  @ApiModelProperty(value = "pais de residencia del cliente")


  public String getPais() {
    return pais;
  }

  public void setPais(String pais) {
    this.pais = pais;
  }

  public Direccion nacionalidad(String nacionalidad) {
    this.nacionalidad = nacionalidad;
    return this;
  }

  /**
   * nacionalidad del cliente
   * @return nacionalidad
  */
  @ApiModelProperty(value = "nacionalidad del cliente")


  public String getNacionalidad() {
    return nacionalidad;
  }

  public void setNacionalidad(String nacionalidad) {
    this.nacionalidad = nacionalidad;
  }

  public Direccion ocupacion(String ocupacion) {
    this.ocupacion = ocupacion;
    return this;
  }

  /**
   * ocupacion del cliente
   * @return ocupacion
  */
  @ApiModelProperty(value = "ocupacion del cliente")


  public String getOcupacion() {
    return ocupacion;
  }

  public void setOcupacion(String ocupacion) {
    this.ocupacion = ocupacion;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Direccion direccion = (Direccion) o;
    return Objects.equals(this.comuna, direccion.comuna) &&
        Objects.equals(this.region, direccion.region) &&
        Objects.equals(this.casaDepto, direccion.casaDepto) &&
        Objects.equals(this.calle, direccion.calle) &&
        Objects.equals(this.numero, direccion.numero) &&
        Objects.equals(this.pais, direccion.pais) &&
        Objects.equals(this.nacionalidad, direccion.nacionalidad) &&
        Objects.equals(this.ocupacion, direccion.ocupacion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(comuna, region, casaDepto, calle, numero, pais, nacionalidad, ocupacion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Direccion {\n");
    
    sb.append("    comuna: ").append(toIndentedString(comuna)).append("\n");
    sb.append("    region: ").append(toIndentedString(region)).append("\n");
    sb.append("    casaDepto: ").append(toIndentedString(casaDepto)).append("\n");
    sb.append("    calle: ").append(toIndentedString(calle)).append("\n");
    sb.append("    numero: ").append(toIndentedString(numero)).append("\n");
    sb.append("    pais: ").append(toIndentedString(pais)).append("\n");
    sb.append("    nacionalidad: ").append(toIndentedString(nacionalidad)).append("\n");
    sb.append("    ocupacion: ").append(toIndentedString(ocupacion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

