package com.cliente.model;

import java.util.Objects;
import com.cliente.model.Cliente;
import com.cliente.model.Notificacion;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ResponseExitoso
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-04-21T16:44:18.089-04:00[America/Santiago]")
public class ResponseExitoso   {
  @JsonProperty("cliente")
  private Cliente cliente;

  @JsonProperty("notificacion")
  private Notificacion notificacion;

  public ResponseExitoso cliente(Cliente cliente) {
    this.cliente = cliente;
    return this;
  }

  /**
   * Get cliente
   * @return cliente
  */
  @ApiModelProperty(value = "")

  @Valid

  public Cliente getCliente() {
    return cliente;
  }

  public void setCliente(Cliente cliente) {
    this.cliente = cliente;
  }

  public ResponseExitoso notificacion(Notificacion notificacion) {
    this.notificacion = notificacion;
    return this;
  }

  /**
   * Get notificacion
   * @return notificacion
  */
  @ApiModelProperty(value = "")

  @Valid

  public Notificacion getNotificacion() {
    return notificacion;
  }

  public void setNotificacion(Notificacion notificacion) {
    this.notificacion = notificacion;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResponseExitoso responseExitoso = (ResponseExitoso) o;
    return Objects.equals(this.cliente, responseExitoso.cliente) &&
        Objects.equals(this.notificacion, responseExitoso.notificacion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cliente, notificacion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResponseExitoso {\n");
    
    sb.append("    cliente: ").append(toIndentedString(cliente)).append("\n");
    sb.append("    notificacion: ").append(toIndentedString(notificacion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

