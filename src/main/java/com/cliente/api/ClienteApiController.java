package com.cliente.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-04-21T16:44:18.089-04:00[America/Santiago]")
@Controller
@RequestMapping("${openapi.sampleClientFlowOAuth2Project.base-path:/Servicio-cliente/1.0.0}")
public class ClienteApiController implements ClienteApi {

    private final NativeWebRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public ClienteApiController(NativeWebRequest request) {
        this.request = request;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

}
